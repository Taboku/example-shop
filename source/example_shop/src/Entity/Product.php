<?php

namespace App\Entity;

use App\Repository\ProductRepository;
use Carbon\Carbon;
use Doctrine\ORM\Mapping as ORM;
use OpenApi\Annotations as OA;
use Ramsey\Uuid\Doctrine\UuidGenerator;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=ProductRepository::class)
 * @ORM\HasLifecycleCallbacks
 * @OA\Schema(required={"name", "description", "price_in_cents", "currency"})
 */
class Product
{
    /**
     * @var \Ramsey\Uuid\UuidInterface
     *
     * @ORM\Id
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=UuidGenerator::class)
     * @OA\Property(type="uuid", minLength=36, maxLength=36, example="1b234d12-9a56-4f18-9ccd-2924ffc7eafc")
     * @Groups("product:get")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"product:get","product:create"})
     *
     * @Assert\NotBlank(groups={"product:create"})
     * @Assert\Length(max=255, groups={"product:create"})
     */
    private $name;

    /**
     * @ORM\Column(type="text")
     * @Groups({"product:get","product:create"})
     *
     * @Assert\NotBlank(groups={"product:create"})
     * @Assert\Length(min=100, max=6000, groups={"product:create"})
     */
    private $description;

    /**
     * @var int|string|null
     * @ORM\Column(type="integer")
     * @Groups({"product:get", "product:create"})
     *
     * @Assert\NotBlank(groups={"product:create"})
     * @Assert\GreaterThan(value=1, groups={"product:create"})
     */
    private $price_in_cents;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"product:get", "product:create"})
     * @OA\Property(type="string", enum={App\ValueObject\Currency::CURRENCY_USD, App\ValueObject\Currency::CURRENCY_EUR, App\ValueObject\Currency::CURRENCY_CBL})
     *
     * @Assert\NotBlank(groups={"product:create"})
     * @Assert\Choice(choices={
     *          App\ValueObject\Currency::CURRENCY_USD,
     *          App\ValueObject\Currency::CURRENCY_EUR,
     *          App\ValueObject\Currency::CURRENCY_CBL,
     *     },
     *     groups={"product:create"}
     * )
     */
    private $currency;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"product:get"})
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updated_at;

    public function getId(): ?UuidInterface
    {
        return Uuid::fromString($this->id);
    }

    public function setId(UuidInterface $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getPriceInCents(): ?int
    {
        return $this->price_in_cents;
    }

    public function setPriceInCents(int $price_in_cents): self
    {
        $this->price_in_cents = $price_in_cents;

        return $this;
    }

    public function getCurrency(): ?string
    {
        return $this->currency;
    }

    public function setCurrency(string $currency): self
    {
        $this->currency = $currency;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updateTimestamps(): void
    {
        $this->setUpdatedAt(Carbon::now());
        if (null === $this->getCreatedAt()) {
            $this->setCreatedAt(Carbon::now());
        }

        if (!$this->id) {
            $this->id = Uuid::uuid4();
        }
    }
}
