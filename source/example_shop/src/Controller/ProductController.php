<?php

namespace App\Controller;

use App\Entity\Product;
use App\Event\ProductCreatedEvent;
use App\Repository\ProductRepository;
use App\ValueObject\Currency;
use Doctrine\ORM\EntityManagerInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;
use Ramsey\Uuid\Uuid;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class ProductController
{
    private ProductRepository $productRepository;

    private EntityManagerInterface $entityManager;

    private SerializerInterface $serializer;

    private TranslatorInterface $translator;

    private ValidatorInterface $validator;

    private EventDispatcherInterface $eventDispatcher;

    public function __construct(
        ProductRepository $productRepository,
        EntityManagerInterface $entityManager,
        SerializerInterface $serializer,
        TranslatorInterface $translator,
        ValidatorInterface $validator,
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->productRepository = $productRepository;
        $this->entityManager = $entityManager;
        $this->serializer = $serializer;
        $this->translator = $translator;
        $this->validator = $validator;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @Route("/api/v1/products", name="get_products", methods={"GET"})
     * @OA\Response(
     *     response=200,
     *     description="Returns a list of products",
     *     @OA\JsonContent(
     *        type="object",
     *        @OA\Property(property="items", type="array", @OA\Items(ref=@Model(type=Product::class, groups={"product:get"}))),
     *        @OA\Property(property="totalItemsCount", type="number", example="35")
     *    )
     * )
     * @OA\Parameter(
     *     name="pageNumber",
     *     in="query",
     *     description="Page Number, default=1",
     *     required=false,
     *     @OA\Schema(type="int")
     * )
     * @OA\Tag(name="Products")
     */
    public function getProducts(Request $request): JsonResponse
    {
        $pageNumber = max((int) $request->get('pageNumber'), 1);

        $products = $this->productRepository->findAllByDate($pageNumber);
        $results = $this->serializer->serialize($products, 'json', ['groups' => 'product:get']);

        return new JsonResponse([
            'items' => json_decode($results, true),
            'totalItemsCount' => $this->productRepository->getTotalItemsCount(),
        ], Response::HTTP_OK);
    }

    /**
     * @Route("/api/v1/products", name="post_product", methods={"POST"})
     * @OA\Response(
     *     response=200,
     *     description="Product has been created succesfully",
     *     @OA\JsonContent(
     *        type="object",
     *        @OA\Property(property="item", type="object", @OA\Property(property="id", type="string", example="67aefb8f-ff00-4d15-9a6c-550e8ff0e12b"))
     *    )
     * )
     *   @OA\RequestBody(
     *       required=true,
     *       description="Created user object",
     *       @OA\MediaType(
     *           mediaType="multipart/form-data",
     *           @OA\Schema(ref=@Model(type=Product::class, groups={"product:create"}))
     *       )
     *   ),
     * @OA\Tag(name="Products")
     */
    public function createProduct(Request $request): JsonResponse
    {
        /** @var Product $product */
        $product = $this->serializer->deserialize(
            json_encode($request->request->all()),
            Product::class,
            'json',
            [AbstractObjectNormalizer::DISABLE_TYPE_ENFORCEMENT => true]
        );

        $errors = $this->validator->validate($product, null, ['product:create']);

        if (count($errors) > 0) {
            return $this->getResponseWithErrors($errors);
        }

        $this->entityManager->persist($product);
        $this->entityManager->flush();

        $this->eventDispatcher->dispatch(new ProductCreatedEvent($product), ProductCreatedEvent::NAME);

        return new JsonResponse(['item' => ['id' => $product->getId()->toString()]], Response::HTTP_OK);
    }

    /**
     * @Route("/api/v1/products/populate", name="populate_products", methods={"GET"})
     * @OA\Response(
     *     response=200,
     *     description="Purge the products database and populate it with 35 random ones",
     *     @OA\JsonContent(
     *      example="[]"
     *    )
     * )
     * @OA\Tag(name="Products")
     */
    public function populateProducts(): JsonResponse
    {
        $this->productRepository->removeAllProducts();

        for ($iterator = 0; $iterator < 35; ++$iterator) {
            $product = new Product();
            $product
                ->setId(Uuid::uuid4())
                ->setDescription('example description '.$iterator)
                ->setName('example name '.$iterator)
                ->setPriceInCents(rand(100, 10000))
                ->setCurrency(Currency::getAvailableCurrencies()[rand(0, 2)]);

            $this->entityManager->persist($product);
        }

        $this->entityManager->flush();

        return new JsonResponse([], Response::HTTP_OK);
    }

    /**
     * @param iterable|ConstraintViolationListInterface[] $errors
     */
    private function getResponseWithErrors(iterable $errors): JsonResponse
    {
        $errorsResponse = [];
        foreach ($errors as $error) {
            $errorField = $error->getPropertyPath();
            if (!isset($errorsResponse[$errorField])) {
                $errorsResponse[$errorField] = [];
            }
            $errorsResponse[$errorField][] = $error->getMessage();
        }

        return new JsonResponse($errorsResponse, Response::HTTP_BAD_REQUEST);
    }
}
