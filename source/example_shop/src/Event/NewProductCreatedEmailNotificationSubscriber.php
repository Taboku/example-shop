<?php

namespace App\Event;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

class NewProductCreatedEmailNotificationSubscriber implements EventSubscriberInterface
{
    private MailerInterface $mailer;

    public function __construct(MailerInterface $mailer)
    {
        $this->mailer = $mailer;
    }

    public static function getSubscribedEvents()
    {
        return [
            ProductCreatedEvent::NAME => 'sendEmail',
        ];
    }

    public function sendEmail(ProductCreatedEvent $event)
    {
        $email = (new Email())
            ->from('example-shop@mailhog.local')
            ->to('maciej.b.glowacki@gmail.com')
            ->subject('New product added')
            ->text('Good news! Someone created a new product in our database! Let\'s celebrate!')
            ->html('<p><b>Good news! Someone created a new product in our database! Let\'s celebrate!</b></p>');

        $this->mailer->send($email);
    }
}
