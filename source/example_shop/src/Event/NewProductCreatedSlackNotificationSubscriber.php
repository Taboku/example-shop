<?php

namespace App\Event;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Notifier\ChatterInterface;
use Symfony\Component\Notifier\Message\ChatMessage;

class NewProductCreatedSlackNotificationSubscriber implements EventSubscriberInterface
{
    private ChatterInterface $chatter;

    public function __construct(ChatterInterface $chatter)
    {
        $this->chatter = $chatter;
    }

    public static function getSubscribedEvents()
    {
        return [
            ProductCreatedEvent::NAME => 'sendNotification',
        ];
    }

    public function sendNotification(ProductCreatedEvent $event)
    {
        $message = (new ChatMessage('Awesome news! Someone added a new product to the database!'))
            ->transport('discord');

        $this->chatter->send($message);
    }
}
