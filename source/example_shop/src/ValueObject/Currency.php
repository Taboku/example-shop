<?php

namespace App\ValueObject;

class Currency
{
    const CURRENCY_USD = 'usd';

    const CURRENCY_EUR = 'eur';

    const CURRENCY_CBL = 'cbl';

    /** @return array|string[] */
    public static function getAvailableCurrencies(): array
    {
        return [self::CURRENCY_USD, self::CURRENCY_EUR, self::CURRENCY_CBL];
    }
}
