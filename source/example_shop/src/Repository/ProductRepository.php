<?php

namespace App\Repository;

use App\Entity\Product;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Product::class);
    }

    public function findAllByDate(int $pageNumber = 1, int $limit = 10)
    {
        return $this->createQueryBuilder('p')
            ->orderBy('p.created_at', 'ASC')
            ->setMaxResults($limit)
            ->setFirstResult($limit * ($pageNumber - 1))
            ->getQuery()
            ->getResult();
    }

    public function getTotalItemsCount(): int
    {
        return $this
            ->getEntityManager()
            ->createQueryBuilder()
            ->select('count(product.id)')
            ->from(Product::class, 'product')
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function removeAllProducts(): void
    {
        $products = $this->findAll();

        foreach ($products as $product) {
            $this->getEntityManager()->remove($product);
        }

        $this->getEntityManager()->flush();
    }
}
