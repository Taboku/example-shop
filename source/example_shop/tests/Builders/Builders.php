<?php

namespace App\Tests\Builders;

use Doctrine\ORM\EntityManagerInterface;

class Builders
{
    /** @var EntityManagerInterface */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function aProduct(): ProductBuilder
    {
        return new ProductBuilder($this->entityManager);
    }
}
