<?php

namespace App\Tests\Builders;

use App\Entity\Product;
use App\ValueObject\Currency;
use Doctrine\ORM\EntityManagerInterface;
use Ramsey\Uuid\Uuid;

class ProductBuilder
{
    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var int */
    private static $seed = 1001;

    /** @var string */
    private $name;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function withName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function build(): Product
    {
        $product = new Product();
        $product
            ->setId(Uuid::uuid4())
            ->setDescription('example description '.self::$seed)
            ->setName($this->name ?? 'example name '.self::$seed)
            ->setPriceInCents(rand(100, 10000))
            ->setCurrency(Currency::getAvailableCurrencies()[rand(0, 2)]);

        $this->entityManager->persist($product);
        $this->entityManager->flush();

        ++self::$seed;

        return $product;
    }
}
