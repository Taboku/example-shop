<?php

namespace App\Tests;

use App\Entity\Product;
use App\Repository\ProductRepository;
use App\Tests\Builders\Builders;
use Carbon\Carbon;
use Doctrine\ORM\EntityManagerInterface;
use Prophecy\Prophet;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class BaseWebTestCase extends WebTestCase
{
    protected KernelBrowser $client;

    protected EntityManagerInterface $entityManager;

    protected Builders $builders;

    protected Prophet $prophet;

    protected function getJsonResponse(): array
    {
        return json_decode($this->client->getResponse()->getContent(), true);
    }

    protected function setUp(): void
    {
        $this->client = self::createClient();
        $container = $this->client->getContainer();
        $this->entityManager = $container->get('doctrine.orm.entity_manager');
        $this->builders = new Builders($this->entityManager);

        /** @var ProductRepository $productRepository */
        $productRepository = $this->entityManager->getRepository(Product::class);
        $productRepository->removeAllProducts();
        $this->prophet = new Prophet();
    }

    protected function tearDown(): void
    {
        $this->prophet->checkPredictions();
        parent::tearDown();
        $this->entityManager->close();
        Carbon::setTestNow();
    }
}
