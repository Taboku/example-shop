<?php

namespace App\Tests\Functional;

use App\Entity\Product;
use App\Tests\BaseWebTestCase;
use Carbon\Carbon;
use Symfony\Component\Mailer\DataCollector\MessageDataCollector;

class ProductTest extends BaseWebTestCase
{
    /** @test */
    public function itShowsListedProducts()
    {
        Carbon::setTestNow(Carbon::createFromFormat('Y-m-d H:i:s', '2020-12-18 15:30:30'));

        $this->builders->aProduct()->withName('Dreadful Hamster')->build();

        $this->client->request('GET', '/api/v1/products');

        $jsonResponse = $this->getJsonResponse();

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertEquals(1, $jsonResponse['totalItemsCount']);
        $this->assertEquals('Dreadful Hamster', $jsonResponse['items'][0]['name']);
        $this->assertEquals('2020-12-18T15:30:30.000000Z', $jsonResponse['items'][0]['created_at']);
    }

    /** @test */
    public function itShowsMultipleProductsWithPagination()
    {
        for ($iterator = 0; $iterator < 35; ++$iterator) {
            $this->builders->aProduct()->withName('example name '.$iterator)->build();
        }

        $this->client->request('GET', '/api/v1/products');
        $jsonResponse = $this->getJsonResponse();

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $this->assertEquals(10, count($jsonResponse['items']));
        $this->assertEquals('example name 0', $jsonResponse['items'][0]['name']);
        $this->assertEquals(35, $jsonResponse['totalItemsCount']);

        $this->client->request('GET', '/api/v1/products?pageNumber=4');
        $jsonResponse = $this->getJsonResponse();

        $this->assertEquals(5, count($jsonResponse['items']));
        $this->assertEquals('example name 30', $jsonResponse['items'][0]['name']);
        $this->assertEquals(35, $jsonResponse['totalItemsCount']);
    }

    /** @test */
    public function itCreatedProduct()
    {
        $this->client->request('POST', '/api/v1/products', [
            'name' => 'example name',
            'description' => 'example description example description example description example description example description example description example description example description example description ',
            'price_in_cents' => 12,
            'currency' => 'usd',
        ]);

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $allItemsInDatabase = $this->entityManager->getRepository(Product::class)->findAll();

        $this->assertEquals(1, count($allItemsInDatabase));
    }

    /** @test */
    public function itDoesNotAllowCreateProductWithInvalidValues()
    {
        $this->client->request('POST', '/api/v1/products', [
            'name' => '',
            'description' => 'too short stuff',
            'price_in_cents' => 0,
            'currency' => 'bln',
        ]);

        $this->assertEquals(400, $this->client->getResponse()->getStatusCode());
        $jsonResponse = $this->getJsonResponse();

        $this->assertEquals('This value should not be blank.', $jsonResponse['name'][0]);
        $this->assertEquals('This value is too short. It should have 100 characters or more.', $jsonResponse['description'][0]);
        $this->assertEquals('This value should be greater than 1.', $jsonResponse['price_in_cents'][0]);
        $this->assertEquals('The value you selected is not a valid choice.', $jsonResponse['currency'][0]);

        $allItemsInDatabase = $this->entityManager->getRepository(Product::class)->findAll();

        $this->assertEquals(0, count($allItemsInDatabase));
    }

    /** @test */
    public function itShouldSendEmailNotificationWhenNewProjectIsCreated()
    {
        $this->client->enableProfiler();

        $this->client->request('POST', '/api/v1/products', [
            'name' => 'example name',
            'description' => 'example description example description example description example description example description example description example description example description example description ',
            'price_in_cents' => 12,
            'currency' => 'usd',
        ]);

        /** @var MessageDataCollector $mailCollector */
        $mailCollector = $this->client->getProfile()->getCollector('mailer');
        $notificationsCollector = $this->client->getProfile()->getCollector('notifier');

        $sentEmails = $mailCollector->getEvents()->getMessages();
        $sentNotifications = $notificationsCollector->getEvents()->getMessages();

        $this->assertEquals(1, count($sentEmails));
        $this->assertEquals(1, count($sentNotifications));
        $this->assertContains(
            'Good news! Someone created a new product in our database! Let\'s celebrate!',
            $sentEmails[0]->toString())
        ;
        $this->assertContains(
            'Awesome news! Someone added a new product to the database!',
            $sentNotifications[0]->getSubject())
        ;
    }
}
